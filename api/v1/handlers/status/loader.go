package status

import (
	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
)

func LoadRoutes(r *fizz.RouterGroup) {
	sg := r.Group("/status", "Status", "Status of the API")

	sg.GET("", []fizz.OperationOption{
		fizz.Summary("Gets status of the service"),
	}, tonic.Handler(GetStatus, 200))

}
