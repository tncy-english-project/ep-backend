package status

import "github.com/gin-gonic/gin"

type Status struct {
	Success bool   `json:"success"`
	Status  string `json:"status"`
}

func GetStatus(c *gin.Context, params *struct{}) (*Status, error) {
	return &Status{
		Success: true,
		Status:  "OK",
	}, nil
}
