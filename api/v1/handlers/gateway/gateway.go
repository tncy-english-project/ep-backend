package gateway

import (
	"ep-backend/internal/models"
	"net/http"
	"sync"

	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

func Gateway(c *gin.Context, params *struct{}) error {
	// We need to upgrade the connection to a websocket
	// and then we need to handle the websocket connection

	user := c.MustGet("user").(*models.User)
	handler(c.Writer, c.Request, user)
	return nil
}

var upgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
}

func handler(w http.ResponseWriter, r *http.Request, user *models.User) {
	conn, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return
	}

	// Add the connection to the room
	if _, ok := Rooms[user.ID]; !ok {
		Rooms[user.ID] = &Room{
			clients: []*websocket.Conn{},
			Mutex:   &sync.Mutex{},
		}
	}

	Rooms[user.ID].AddClient(conn)

	for {
		_, msg, err := conn.ReadMessage()
		if err != nil {
			// Remove the connection from the room
			Rooms[user.ID].RemoveClient(conn)
			break
		}
		p, err := NewPacketFromBytes(msg)
		if err != nil {
			// Send an error packet
			conn.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
			continue
		}

		switch p.Type {
		case PacketTypePing:
			// Send a pong packet
			conn.WriteMessage(websocket.TextMessage, NewPongPacket("pong").ToBytes())
		case PacketTypeWAStart:
			// Send a game start packet
			handleWAStart(conn, user)
		case PacketTypeWAEnd:
			// Send a game end packet
			handleWAEnd(conn, user, p)
		}
	}
}
