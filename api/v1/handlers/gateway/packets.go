package gateway

import (
	"encoding/json"
	"ep-backend/internal/models"
)

type PacketType string

const (
	PacketTypePing       PacketType = "ping"
	PacketTypePong       PacketType = "pong"
	PacketTypeErr        PacketType = "error"
	PacketTypeWAStart    PacketType = "wa-start"
	PacketTypeWARedirect PacketType = "wa-redirect"
	PacketTypeWAEnd      PacketType = "wa-end"
	PacketTypeWAEnded    PacketType = "wa-ended"
)

type Packet struct {
	Type    PacketType  `json:"type"`
	Payload interface{} `json:"payload"`
	Raw     []byte      `json:"-"`
}

func (p *Packet) ToBytes() []byte {
	data, _ := json.Marshal(p)
	return data
}

func NewPacket(t PacketType, payload interface{}) *Packet {
	return &Packet{
		Type:    t,
		Payload: payload,
	}
}

func NewPacketFromBytes(data []byte) (*Packet, error) {
	p := &Packet{}
	err := json.Unmarshal(data, p)
	if err != nil {
		return nil, err
	}
	p.Raw = data
	return p, nil
}

type PingPayload struct {
	Message string `json:"message"`
}

type PongPayload struct {
	Message string `json:"message"`
}

type ErrorPayload struct {
	Message string `json:"message"`
}

type WARedirectPayload struct {
	Game *models.WordAssociation `json:"game"`
}

type WAEndPayload struct {
	Game *models.WordAssociation `json:"game"`
}

func NewPingPacket(message string) *Packet {
	return NewPacket(PacketTypePing, &PingPayload{
		Message: message,
	})
}

func NewPongPacket(message string) *Packet {
	return NewPacket(PacketTypePong, &PongPayload{
		Message: message,
	})
}

func NewErrorPacket(message string) *Packet {
	return NewPacket(PacketTypeErr, &ErrorPayload{
		Message: message,
	})
}

func NewWARedirectPacket(game *models.WordAssociation) *Packet {
	return NewPacket(PacketTypeWARedirect, &WARedirectPayload{
		Game: game,
	})
}
