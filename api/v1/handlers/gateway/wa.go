package gateway

import (
	"encoding/json"
	"ep-backend/internal/models"
	"io"
	"net/http"
	"os"
	"strings"
	"time"

	"github.com/gorilla/websocket"
)

type APIResponse struct {
	Entry              string                 `json:"entry"`
	Request            string                 `json:"request"`
	Response           map[string]interface{} `json:"response"`
	Associations       string                 `json:"associations"`
	AssociationsArr    []string               `json:"associations_array"`
	AssociationsScores map[string]float64     `json:"associations_scored"`
	Version            string                 `json:"version"`
	Author             string                 `json:"author"`
	Email              string                 `json:"email"`
	ResultCode         string                 `json:"result_code"`
	ResultMsg          string                 `json:"result_msg"`
}

func handleWAStart(c *websocket.Conn, u *models.User) {
	// We check that the user doesn't already have a game going on, if he has, we redirect him to the game
	// If he doesn't, we create a new game and redirect him to it
	w, err := u.GetActiveWordAssociations()
	if err != nil {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
		return
	}

	if len(w) > 0 {
		c.WriteMessage(websocket.TextMessage, NewWARedirectPacket(w[0]).ToBytes())
		return
	}

	// We find a random word
	word, err := models.GetRandomWord()
	if err != nil {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
		return
	}

	// We create a new game
	var wa = &models.WordAssociation{
		Word:      word.Word,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
		User:      u,
	}

	// We fill it using the API

	url := "https://twinword-word-associations-v1.p.rapidapi.com/associations/"

	payload := strings.NewReader("entry=sound")

	req, err := http.NewRequest("POST", url, payload)
	if err != nil {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
		return
	}

	req.Header.Add("content-type", "application/x-www-form-urlencoded")
	req.Header.Add("X-RapidAPI-Key", os.Getenv("RAPIDAPI_KEY_WA"))
	req.Header.Add("X-RapidAPI-Host", "twinword-word-associations-v1.p.rapidapi.com")

	res, err := http.DefaultClient.Do(req)
	if err != nil {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
		return
	}

	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)

	var apiResponse APIResponse
	err = json.Unmarshal(body, &apiResponse)
	if err != nil {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
		return
	}

	for _, v := range apiResponse.AssociationsArr {
		wa.Solutions = append(wa.Solutions, &models.WordAssociationSolution{
			Word:      v,
			Proximity: apiResponse.AssociationsScores[v],
		})
	}

	err = wa.Create()
	if err != nil {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
		return
	}

	c.WriteMessage(websocket.TextMessage, NewWARedirectPacket(wa).ToBytes())
}

func handleWAEnd(c *websocket.Conn, u *models.User, p *Packet) {
	// Convert packet to a WAEndPayload
	var payload WAEndPayload
	err := json.Unmarshal(p.Raw, &payload)
	if err != nil {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
		return
	}

	// We check that the user has a game going on
	w, err := u.GetActiveWordAssociations()
	if err != nil {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
		return
	}

	if len(w) == 0 {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket("No game found").ToBytes())
		return
	}

	// We check that the game is the same as the one in the payload
	if w[0].ID != payload.Game.ID {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket("Game not found").ToBytes())
		return
	}

	// We check that the game is not already finished
	if w[0].Ended {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket("Game already finished").ToBytes())
		return
	}

	// We only store the answers that are in the solutions
	w[0].Answers = payload.Game.Answers
	w[0].Ended = true
	w[0].UpdatedAt = time.Now()

	err = w[0].Update()
	if err != nil {
		c.WriteMessage(websocket.TextMessage, NewErrorPacket(err.Error()).ToBytes())
		return
	}

	c.WriteMessage(websocket.TextMessage, NewPacket(PacketTypeWAEnded, nil).ToBytes())
}
