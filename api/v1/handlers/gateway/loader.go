package gateway

import (
	"ep-backend/api/v1/middlewares"

	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
)

func LoadRoutes(r *fizz.RouterGroup) {
	sg := r.Group("/gateway", "Gateway", "Websocket gateway")

	sg.GET("/", []fizz.OperationOption{
		fizz.Summary("Connect to the gateway to receive realtime updates (WILL NOT WORK IN SWAGGER), not to be used to send messages"),
		fizz.ResponseWithExamples("400", "Bad request", nil, nil, map[string]interface{}{
			"error": "Bad request",
		}),
	}, middlewares.AuthRequired(), tonic.Handler(Gateway, 200))
}
