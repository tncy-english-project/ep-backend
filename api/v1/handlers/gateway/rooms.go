package gateway

import (
	"encoding/json"
	"sync"

	"github.com/gorilla/websocket"
)

type Room struct {
	clients []*websocket.Conn
	*sync.Mutex
}

var Rooms = make(map[string]*Room)

func (r *Room) AddClient(c *websocket.Conn) {
	r.Lock()
	r.clients = append(r.clients, c)
	r.Unlock()
}

func (r *Room) RemoveClient(c *websocket.Conn) {
	for i, client := range r.clients {
		if client == c {
			r.Lock()
			r.clients = append(r.clients[:i], r.clients[i+1:]...)
			r.Unlock()
		}
	}
}

func (r *Room) Broadcast(msg interface{}) {
	data, _ := json.Marshal(msg)
	for _, client := range r.clients {
		client.WriteMessage(websocket.TextMessage, data)
	}
}
