package auth

import (
	"ep-backend/api/v1/middlewares"

	"github.com/loopfz/gadgeto/tonic"
	"github.com/wI2L/fizz"
)

func LoadRoutes(r *fizz.RouterGroup) {
	sg := r.Group("/auth", "Auth", "Authentication routes")

	sg.POST("/register", []fizz.OperationOption{
		fizz.Summary("Register a new user"),
		fizz.ResponseWithExamples("400", "Bad request", nil, nil, map[string]interface{}{
			"error": "Bad request",
		}),
	}, middlewares.DeauthRequired(), tonic.Handler(Register, 200))
	sg.POST("/login", []fizz.OperationOption{
		fizz.Summary("Login the current user"),
		fizz.ResponseWithExamples("400", "Bad request", nil, nil, map[string]interface{}{
			"error": "Bad request",
		}),
	}, middlewares.DeauthRequired(), tonic.Handler(Login, 200))
	sg.GET("/logout", []fizz.OperationOption{
		fizz.Summary("Logout the current user"),
		fizz.ResponseWithExamples("400", "Bad request", nil, nil, map[string]interface{}{
			"error": "Bad request",
		}),
	}, middlewares.AuthRequired(), tonic.Handler(Logout, 200))
	sg.GET("/connected", []fizz.OperationOption{
		fizz.Summary("Check if the current user is connected"),
		fizz.ResponseWithExamples("400", "Bad request", nil, nil, map[string]interface{}{
			"error": "Bad request",
		}),
	}, middlewares.AuthRequired(), tonic.Handler(Connected, 200))
}
