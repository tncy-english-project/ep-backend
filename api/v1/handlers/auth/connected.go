package auth

import (
	"ep-backend/internal/models"

	"github.com/gin-gonic/gin"
)

// ConnectedOutput is the struct that will be used to return the response
type ConnectedOutput struct {
	Success bool         `json:"success"`
	User    *models.User `json:"user"`
}

func Connected(c *gin.Context, params *struct{}) (*ConnectedOutput, error) {
	user := c.MustGet("user").(*models.User)

	return &ConnectedOutput{
		Success: true,
		User:    user,
	}, nil
}
