package auth

import (
	"ep-backend/internal/models"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
)

// LoginInpout is the struct that will be used to bind the request body
type LoginInpout struct {
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

// LoginOutput is the struct that will be used to return the response
type LoginOutput struct {
	Success bool         `json:"success"`
	Status  string       `json:"status"`
	User    *models.User `json:"user"`
}

func Login(c *gin.Context, f *LoginInpout) (*LoginOutput, error) {
	// Check if the user exists by username or email
	user, err := models.GetUserByUsername(f.Username)
	if err != nil {
		user, err = models.GetUserByEmail(f.Username)
		if err != nil {
			return nil, errors.BadRequestf("User not found or password incorrect")
		}
	}

	// Check if the password is correct
	if !user.CheckPasswordHash(f.Password) {
		return nil, errors.BadRequestf("User not found or password incorrect")
	}

	// Store the user ID in the session
	session := sessions.Default(c)
	session.Set("user", user.ID)
	session.Save()

	return &LoginOutput{
		Success: true,
		Status:  "Vous êtes maintenant connecté",
		User:    user,
	}, nil

}
