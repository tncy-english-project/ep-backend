package auth

import (
	"ep-backend/internal/ids"
	"ep-backend/internal/models"
	"regexp"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/juju/errors"
)

// RegisterInput is the struct that will be used to bind the request body
type RegisterInput struct {
	Username       string `json:"username" binding:"required"`
	Email          string `json:"email" binding:"required"`
	Password       string `json:"password" binding:"required"`
	PasswordVerify string `json:"password_verify" binding:"required"`
}

// RegisterOutput is the struct that will be used to return the response
type RegisterOutput struct {
	Success bool   `json:"success"`
	Status  string `json:"status"`
}

var (
	usernameRegex = regexp.MustCompile(`^[a-zA-Z0-9_]{3,16}$`)
	emailRegex    = regexp.MustCompile(`^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$`)
	passwordRegex = regexp.MustCompile(`^[a-zA-Z0-9!@#$%^&*()_+-=]{8,32}$`)
)

func Register(c *gin.Context, f *RegisterInput) (*RegisterOutput, error) {
	// Check if the passwords match
	if f.Password != f.PasswordVerify {
		return nil, errors.BadRequestf("Passwords don't match")
	}

	// Check if the username is already taken
	if _, err := models.GetUserByUsername(f.Username); err == nil {
		return nil, errors.BadRequestf("Username already taken")
	}

	// Check if the email is already taken
	if _, err := models.GetUserByEmail(f.Email); err == nil {
		return nil, errors.BadRequestf("Email already taken")
	}

	// Verify the fields with regex
	if !usernameRegex.MatchString(f.Username) {
		return nil, errors.BadRequestf("Invalid username, must be between 3 and 16 characters and contain only letters, numbers and _")
	}

	if !emailRegex.MatchString(f.Email) {
		return nil, errors.BadRequestf("Invalid email, must be a valid email address")
	}

	if !passwordRegex.MatchString(f.Password) {
		return nil, errors.BadRequestf("Invalid password, must be between 8 and 32 characters and contain only letters, numbers and special characters")
	}

	// Hash the password
	pHash, err := models.HashPassword(f.Password)
	if err != nil {
		return nil, errors.Errorf("Error while hashing password")
	}

	// Create the user
	user := models.User{
		ID:        ids.Generate(),
		Username:  f.Username,
		Email:     f.Email,
		Password:  pHash,
		CreatedAt: time.Now(),
		UpdatedAt: time.Now(),
	}

	// Save the user in the database
	if err := user.Create(); err != nil {
		return nil, errors.Errorf("Error while creating user")
	}

	return &RegisterOutput{
		Success: true,
		Status:  "Account created",
	}, nil
}
