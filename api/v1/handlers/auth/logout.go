package auth

import (
	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

type LogoutOutput struct {
	Success bool   `json:"success"`
	Status  string `json:"status"`
}

func Logout(c *gin.Context, params *struct{}) (*LogoutOutput, error) {
	session := sessions.Default(c)
	session.Clear()
	session.Save()
	return &LogoutOutput{
		Success: true,
		Status:  "Vous êtes maintenant déconnecté",
	}, nil
}
