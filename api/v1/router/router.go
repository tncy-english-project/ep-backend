package router

import (
	"ep-backend/api/v1/handlers/auth"
	"ep-backend/api/v1/handlers/gateway"
	"ep-backend/api/v1/handlers/status"

	"github.com/wI2L/fizz"
)

func Route(engine *fizz.Fizz) {
	path := engine.Group("/api/v1", "API v1", "API v1 routes")

	auth.LoadRoutes(path)
	status.LoadRoutes(path)
	gateway.LoadRoutes(path)
}
