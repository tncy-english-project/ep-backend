package db

import (
	"database/sql"
	"ep-backend/internal/env"
	"log"

	_ "github.com/go-sql-driver/mysql"
	"github.com/google/uuid"
)

var DB *sql.DB

func init() {
	// Open up our database connection.
	db, err := sql.Open("mysql", env.Config.MysqlDSN())
	if err != nil {
		panic(err.Error())
	}

	// Connect and check the server version
	var version string
	err = db.QueryRow("SELECT VERSION()").Scan(&version)
	if err != nil {
		panic(err.Error())
	}
	log.Println("[EP-DB] Connected to MySQL:", version)

	DB = db
	createTables()
}

// createTables creates the tables if they don't exist
func createTables() {
	// Create users table
	_, err := DB.Exec(`CREATE TABLE IF NOT EXISTS users (
		id VARCHAR(255) NOT NULL,
		username VARCHAR(255) NOT NULL,
		password VARCHAR(255) NOT NULL,
		email VARCHAR(255) NOT NULL,
		created_at DATETIME NOT NULL,
		updated_at DATETIME NOT NULL,
		PRIMARY KEY (id)
	)`)
	if err != nil {
		panic(err.Error())
	}

	// Create word associations table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS word_associations (
		id VARCHAR(255) NOT NULL,
		user_id VARCHAR(255) NOT NULL,
		word VARCHAR(255) NOT NULL,
		ended BOOLEAN NOT NULL,
		created_at DATETIME NOT NULL,
		updated_at DATETIME NOT NULL,
		PRIMARY KEY (id),
		FOREIGN KEY (user_id) REFERENCES users(id)
	)`)
	if err != nil {
		panic(err.Error())
	}

	// Create word association solutions table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS word_association_solutions (
		id VARCHAR(255) NOT NULL,
		word_association_id VARCHAR(255) NOT NULL,
		word VARCHAR(255) NOT NULL,
		proximity DOUBLE NOT NULL,
		PRIMARY KEY (id),
		FOREIGN KEY (word_association_id) REFERENCES word_associations(id)
	)`)
	if err != nil {
		panic(err.Error())
	}

	// Create word association answers table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS word_association_answers (
		id VARCHAR(255) NOT NULL,
		word_association_id VARCHAR(255) NOT NULL,
		word VARCHAR(255) NOT NULL,
		created_at DATETIME NOT NULL,
		PRIMARY KEY (id),
		FOREIGN KEY (word_association_id) REFERENCES word_associations(id)
	)`)
	if err != nil {
		panic(err.Error())
	}

	// Create word table
	_, err = DB.Exec(`CREATE TABLE IF NOT EXISTS words (
		word VARCHAR(255) NOT NULL
	)`)
	if err != nil {
		panic(err.Error())
	}
}

// GenerateID generates a random ID
func GenerateID() string {
	return uuid.New().String()
}
