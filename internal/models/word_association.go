package models

import (
	"ep-backend/internal/db"
	"time"
)

type WordAssociationSolution struct {
	ID        int     `json:"id"`
	Word      string  `json:"word"`
	Proximity float64 `json:"proximity"`
}

type WordAssociationAnswer struct {
	ID   int    `json:"id"`
	Word string `json:"word"`

	CreatedAt time.Time `json:"created_at"`
}

type WordAssociation struct {
	ID        string    `json:"id"`
	Word      string    `json:"word"`
	Ended     bool      `json:"finished"`
	CreatedAt time.Time `json:"created_at"`
	UpdatedAt time.Time `json:"updated_at"`

	Solutions []*WordAssociationSolution `json:"solutions"`
	Answers   []*WordAssociationAnswer   `json:"answers"`
	User      *User                      `json:"user"`
}

func (wa *WordAssociation) Create() error {
	wa.ID = db.GenerateID()
	wa.CreatedAt = time.Now()
	wa.UpdatedAt = time.Now()
	_, err := db.DB.Exec("INSERT INTO word_associations (id, user_id, word, ended, created_at, updated_at) VALUES (?, ?, ?, ?, ?, ?)", wa.ID, wa.User.ID, wa.Word, wa.Ended, wa.CreatedAt, wa.UpdatedAt)
	if err != nil {
		return err
	}

	for i, solution := range wa.Solutions {
		solution.ID = i
		_, err := db.DB.Exec("INSERT INTO word_association_solutions (id, word_association_id, word, proximity) VALUES (?, ?, ?, ?)", solution.ID, wa.ID, solution.Word, solution.Proximity)
		if err != nil {
			return err
		}
	}

	return nil
}

func (wa *WordAssociation) Update() error {
	wa.UpdatedAt = time.Now()
	_, err := db.DB.Exec("UPDATE word_associations SET ended = ?, updated_at = ? WHERE id = ?", wa.Ended, wa.UpdatedAt, wa.ID)
	if err != nil {
		return err
	}

	// Delete old solutions
	_, err = db.DB.Exec("DELETE FROM word_association_solutions WHERE word_association_id = ?", wa.ID)
	if err != nil {
		return err
	}

	// Insert new solutions
	for i, solution := range wa.Solutions {
		solution.ID = i
		_, err := db.DB.Exec("INSERT INTO word_association_solutions (id, word_association_id, word, proximity) VALUES (?, ?, ?, ?)", solution.ID, wa.ID, solution.Word, solution.Proximity)
		if err != nil {
			return err
		}
	}

	// Delete old answers
	_, err = db.DB.Exec("DELETE FROM word_association_answers WHERE word_association_id = ?", wa.ID)
	if err != nil {
		return err
	}

	// Insert new answers
	for _, answer := range wa.Answers {
		_, err := db.DB.Exec("INSERT INTO word_association_answers (word_association_id, word, created_at) VALUES (?, ?, ?)", wa.ID, answer.Word, answer.CreatedAt)
		if err != nil {
			return err
		}
	}

	return nil
}

func (wa *WordAssociation) Finished() bool {
	return len(wa.Answers) == len(wa.Solutions)
}

func (wa *WordAssociation) Score() float64 {
	// Score according to order
	score := 0.0
	for i, solution := range wa.Solutions {
		if wa.Answers[i].Word == solution.Word {
			score += 10 * solution.Proximity
		}
	}

	// Score according to presence
	for _, solution := range wa.Solutions {
		for _, answer := range wa.Answers {
			if answer.Word == solution.Word {
				score += 5 * solution.Proximity
			}
		}
	}

	return score
}

func GetWordAssociationByID(id string) (*WordAssociation, error) {
	var wa WordAssociation
	err := db.DB.QueryRow("SELECT id, user_id, word, ended, created_at, updated_at FROM word_associations WHERE id = ?", id).Scan(&wa.ID, &wa.User.ID, &wa.Word, &wa.Ended, &wa.CreatedAt, &wa.UpdatedAt)
	if err != nil {
		return nil, err
	}

	// Get solutions
	rows, err := db.DB.Query("SELECT id, word, proximity FROM word_association_solutions WHERE word_association_id = ?", id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var solution WordAssociationSolution
		err := rows.Scan(&solution.ID, &solution.Word, &solution.Proximity)
		if err != nil {
			return nil, err
		}
		wa.Solutions = append(wa.Solutions, &solution)
	}

	// Get answers
	rows, err = db.DB.Query("SELECT id, word FROM word_association_answers WHERE word_association_id = ?", id)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	for rows.Next() {
		var answer WordAssociationAnswer
		err := rows.Scan(&answer.ID, &answer.Word)
		if err != nil {
			return nil, err
		}
		wa.Answers = append(wa.Answers, &answer)
	}

	// Get user
	user, err := GetUserByID(wa.User.ID)
	if err != nil {
		return nil, err
	}

	wa.User = user

	return &wa, nil
}

func (user *User) GetWordAssociations() ([]*WordAssociation, error) {
	rows, err := db.DB.Query("SELECT id, user_id, word, ended, created_at, updated_at FROM word_associations WHERE user_id = ?", user.ID)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var was []*WordAssociation
	for rows.Next() {
		var wa WordAssociation
		err := rows.Scan(&wa.ID, &wa.User.ID, &wa.Word, &wa.Ended, &wa.CreatedAt, &wa.UpdatedAt)
		if err != nil {
			return nil, err
		}

		// Get solutions
		rows, err := db.DB.Query("SELECT id, word, proximity FROM word_association_solutions WHERE word_association_id = ?", wa.ID)
		if err != nil {
			return nil, err
		}
		defer rows.Close()

		for rows.Next() {
			var solution WordAssociationSolution
			err := rows.Scan(&solution.ID, &solution.Word, &solution.Proximity)
			if err != nil {
				return nil, err
			}
			wa.Solutions = append(wa.Solutions, &solution)
		}

		// Get answers
		rows, err = db.DB.Query("SELECT id, word FROM word_association_answers WHERE word_association_id = ?", wa.ID)
		if err != nil {
			return nil, err
		}
		defer rows.Close()

		for rows.Next() {
			var answer WordAssociationAnswer
			err := rows.Scan(&answer.ID, &answer.Word)
			if err != nil {
				return nil, err
			}
			wa.Answers = append(wa.Answers, &answer)
		}

		was = append(was, &wa)
	}

	return was, nil
}

func (user *User) GetActiveWordAssociations() ([]*WordAssociation, error) {
	rows, err := db.DB.Query("SELECT id, user_id, word, ended, created_at, updated_at FROM word_associations WHERE user_id = ? AND ended = ?", user.ID, false)
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var was []*WordAssociation
	for rows.Next() {
		var wa WordAssociation
		err := rows.Scan(&wa.ID, &wa.User.ID, &wa.Word, &wa.Ended, &wa.CreatedAt, &wa.UpdatedAt)
		if err != nil {
			return nil, err
		}

		// Get solutions
		rows, err := db.DB.Query("SELECT id, word, proximity FROM word_association_solutions WHERE word_association_id = ?", wa.ID)
		if err != nil {
			return nil, err
		}
		defer rows.Close()

		for rows.Next() {
			var solution WordAssociationSolution
			err := rows.Scan(&solution.ID, &solution.Word, &solution.Proximity)
			if err != nil {
				return nil, err
			}
			wa.Solutions = append(wa.Solutions, &solution)
		}

		// Get answers
		rows, err = db.DB.Query("SELECT id, word FROM word_association_answers WHERE word_association_id = ?", wa.ID)
		if err != nil {
			return nil, err
		}
		defer rows.Close()

		for rows.Next() {
			var answer WordAssociationAnswer
			err := rows.Scan(&answer.ID, &answer.Word)
			if err != nil {
				return nil, err
			}
			wa.Answers = append(wa.Answers, &answer)
		}

		was = append(was, &wa)
	}

	return was, nil
}
