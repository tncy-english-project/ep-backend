package models

import "ep-backend/internal/db"

type Word struct {
	Word string `json:"word"`
}

func NewWord(word string) *Word {
	return &Word{
		Word: word,
	}
}

func (w *Word) Create() error {
	_, err := db.DB.Exec("INSERT INTO words (word) VALUES (?)", w.Word)
	if err != nil {
		return err
	}

	return nil
}

func (w *Word) Delete() error {
	_, err := db.DB.Exec("DELETE FROM words WHERE word = ?", w.Word)
	if err != nil {
		return err
	}

	return nil
}

func GetRandomWord() (*Word, error) {
	rows, err := db.DB.Query("SELECT word FROM words ORDER BY RAND() LIMIT 1")
	if err != nil {
		return nil, err
	}
	defer rows.Close()

	var word string
	for rows.Next() {
		err := rows.Scan(&word)
		if err != nil {
			return nil, err
		}
	}

	return NewWord(word), nil
}
