package main

import (
	"ep-backend/api/v1/router"
	"ep-backend/internal/env"
	"net/http"
	"time"

	_ "embed"

	"github.com/gin-contrib/cors"
	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
	"github.com/wI2L/fizz"
	"github.com/wI2L/fizz/openapi"
)

var secret = []byte(env.Config.CookieKey)

func main() {
	// Start the API server
	engine := gin.New()

	engine.Use(cors.New(cors.Config{
		AllowOrigins:     []string{env.Config.FrontendURL()},
		AllowMethods:     []string{"*"},
		AllowHeaders:     []string{"Origin, X-Requested-With, Content-Type, Accept"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))

	app := fizz.NewFromEngine(engine)

	infos := &openapi.Info{
		Title:       "English Project API",
		Description: `Self explanatory`,
		Version:     "1.0.0",
	}
	// Create a new route that serve the OpenAPI spec.
	app.GET("/openapi.json", nil, app.OpenAPI(infos, "json"))

	// Setup the session
	store := cookie.NewStore(secret)
	store.Options(sessions.Options{
		MaxAge: 10 * 60 * 60 * 24,
		Path:   "/",
	})
	app.Use(sessions.Sessions(env.Config.CookieName, store))

	// Setup the routes
	router.Route(app)

	// Use f on /docs/
	app.Engine().Static("/docs/", "swaggerui")

	// Start the server
	srv := &http.Server{
		Addr:    ":8000",
		Handler: app,
	}
	srv.ListenAndServe()
}
